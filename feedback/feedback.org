* Feedback for Documents till date:
** Project concept document.
    + Description of the idea about the project. Details aren't quite clear in this document. This needs to be updated, as discussed on [2014-09-13 Sat].
** Project Plan Document.
    + Please update the doucment with three iterations as we agreed upon earlier.
    + As I understand we are dealing with the below modules by R1.
        - Code upload portal with UI and testing done by Iteration 1.
        - User Administration module.
    + Modules to be done by R2.
        - Confirmation of bugs and evaluation of code by a judge.
    + Gamification details are not clear.

** SRS
    + [file:usecase.jpg]
    + SRS is ready witht the requirements that could be implemented till R1.
    + Plan for R2 should be clear. Details should be updated in SRS doucment.
** Status Tracker
    + Please update the status tracker with the tasks identified in the Project plan document.

* Scores till date:
** Concept Doucment Max:10 points.
    +  ToBeAnnounced(TBA)
** SRS Max : 30 points.
    + could evaluate only out of 15 points. Use cases related to R2 will be evaluated once they are updated in SRS document.
    + Team performance : 15 points awarded. Fine with the way usecases are recorded and described. If interested, Could write alternate path and exception path also.
** Project Plan Document. Max: 10points
    + Please update the documents with iterations planned and get it re-evaluated.
    + Also Please come up with gamification ideas. Get them approved and finalized from Raghu sir, Update the same in the Project plan document.
    + Update SRS document accordingly.
** Status Document. Max 30 points.
    + Please update the doucment with the tasks. 
    + will be evaluated later. Once all the status reports are ready by [2014-11-10 Mon]

