Meeting Minutes Nov 11th 2:10 pm
================

Attendees:

  - Client Raghu Reddy sir

  - Sai Krishna sir

  - Rashi Shrishrimal

  - Ranjith Reddy

  - Sikander Sharda

  - Smriti Singh


 Scribe:

  - Rashi Shrishrimal	

 Agenda:

  - To demo the work done till now and get feedback of Client. 
 
 Action Points:

> For the upload portal, sir asked to add a help button which would give instructions regarding the format and extension of the file to be uploaded.

> He also told to simplify the format of .csv file and he wanted to change the format but since its not possible within time limit, he asked us to carry on.
> For the page which shows the code to be reviewed, sir asked to add bug description field.

> Also sir told us to add instructions page for the users and to make ui fancy.

 Next meeting:
   
- 13th Nov, 2014

 Next meeting agenda items:

 - To demo parts added after the meeting 

 Adjournment:

 2:40 pm	


 


