Meeting Minutes Nov 4th 11:10 am
================

Attendees:

  - Sai Krishna sir

  - Rashi Shrishrimal

  - Ranjith Reddy


 Scribe:

  - Rashi Shrishrimal	

 Agenda:

  - To demo the work done till now
  
  - To discuss what needs to be done for R2 

  - Get feedback of sir regarding our work flow

 Action Points:

> Sir told us to show our work done till now, but some errors occured midway.

> Sir asked to make a README file which will help the users to install our game and play it.

> Sir was dissatisfied for not maintaining status tracker properly and asked us to update it as soon as possible.

> He also told us to complete the gamification part as soon as possible and to show to sir whatever has been done till now on 7th Nov meeting so that we can take inputs from sir and implement on it.

> Ranjith and sir discussed some ideas which can be implemented for frontend.

 Next meeting:
   
- 5th Nov, 2014

 Next meeting agenda items:

 - To divide work 

 - To discuss what will be shown to sir on 7th Nov meeting

 Adjournment:

  11:50 am	


 


