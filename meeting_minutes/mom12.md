Meeting Minutes Nov 6th 5:00 pm
================

Attendees:

  - Raghu Reddy sir

  - Sai Krishna sir

  - Rashi Shrishrimal

  - Ranjith Reddy

  - Sikander Sharda


 Scribe:

  - Rashi Shrishrimal	

 Agenda:

  - To get his views on the gamification part and to show what we have decided for it 
  
  - Get feedback of sir regarding our work flow

 Action Points:

> We showed the screen to sir deming the gamification part but sir was not happy since some errors ocurred.

> He also told us to complete the gamification part as soon as possible and to show to sir whatever has been done till now on 11th Nov so that he can comment on it.

 Next meeting:
   
- 11th Nov, 2014

 Next meeting agenda items:

 - To demo gamification part

 Adjournment:

  5:30 pm	


 


